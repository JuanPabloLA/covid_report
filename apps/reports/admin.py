from django.contrib import admin
from .models import Countries, Register
# Register your models here.

admin.site.register(Countries)
admin.site.register(Register)
