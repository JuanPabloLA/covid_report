from django.db import models


class Countries(models.Model):
    country = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.country


class Register(models.Model):

    country = models.ForeignKey(Countries, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    total_cases = models.IntegerField(default=0)
    new_cases = models.IntegerField(default=0)
    total_deaths = models.IntegerField(default=0)
    new_deaths = models.IntegerField(default=0)
    total_recovered = models.IntegerField(default=0)
    new_recovered = models.IntegerField(default=0)
    active_cases = models.IntegerField(default=0)
    serious_critical = models.IntegerField(default=0)
    total_cases_per_million = models.IntegerField(default=0)
    deaths_per_million = models.IntegerField(default=0)
    total_tests = models.IntegerField(default=0)
    tests_per_million = models.IntegerField(default=0)
    population = models.IntegerField(default=0)

    def __str__(self):
        return self.country.country
